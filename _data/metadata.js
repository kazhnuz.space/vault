module.exports = {
	title: "Kazhnuz' Vault",
	url: "https://vault.kazhnuz.space/",
	language: "fr",
	description: "Un site pour mes vieux projets abandonnés",
	author: {
		name: "Kazhnuz",
		email: "kazhnuz@kobold.cafe",
		url: "https://kazhnuz.space/"
	}
}
