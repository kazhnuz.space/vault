---
layout: layouts/base.njk
eleventyNavigation:
  key: Uchronie Solaire
  parent: Worldbuilding
  order: 0
models:
  - Uchronie Solaire
title: Uchronie Solaire
---

# Petite histoire uchronique du système solaire

Un petit projet d’univers de SF que j’ai depuis quelques années, et qui est apparu dans des créations comme Zoomorphes et Artifices ! Il s’agit d’un univers de SF uchronique c’est-à-dire que toute sa chronologie découle d’un moment de l’histoire entre cet univers et le nôtre qui s’est passé différemment. Ici, c’est l’accident de challenger qui ne s’est pas produit. J'ai toujours eut une fascination pour cet événement, qui pour moi a été sans doute ce qui a mis un grand frein à l'envie de conquérir les étoiles. Et pour la petite anecdote : je partage ma date d'anniversaire avec cet événement xD

J'avait créé cet univers en premier lieu pour mon projet de roman « Nouvelle Frontière » (un vieux projet de SF que j’avais prévu pour un NaNoWriMo, mais je n’ai pas réussi à écrire pour des raisons diverses), qui devait se passant dans les années 2030 de cet univers alternatif, dans une base antarctique. On pouvait considérer que tous mes écrits de science-fiction se passent dans cet univers.

Je vous présente donc ici un des documents que j’ai créés pour cet univers, qui raconte un peu l’histoire de ce système solaire alternatif.