---
layout: layouts/base.njk
eleventyNavigation:
  key: Worldbuilding
  order: 2
---

# Worldbuilding

Pleins de vieux projets d'univers perso. Pas grand chose à en dire, vous trouverez un peu de tout dans cette catégorie, telle que des anciens projets de JDR, des anciens systèmes, etc. D'anciennes versions aussi potentiellements d'univers.