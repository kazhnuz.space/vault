---
layout: layouts/base.njk
eleventyNavigation:
  key: Imperium Porcorum
  parent: Worldbuilding
  order: 2
models:
  - Imperium Porcorum
title: Imperium Porcorum
---

# Imperium Porcorum

> La planète Ukropie était une planète normale et pleine de vie, jusqu'à ce qu'arrive l'invasion la plus stupide qu'on pouvait imaginer. Une armée de millier de cochons, aux pouvoirs surpuissants, qui se sont abattu sur notre société sérieuse comme une peste rose, rongeant tout ce qu'on avait créé pour en faire un chaos.

Imperium Porcorum est un univers humoristique et parodique, où une armée de cochons surpuissant se sont attaqué à une planète et l'ont envahi, le tout pleins de superpouvoirs absurdes, le tout sur la planète Ukropie.

En l'an 1111, la planète Urkopie a été envahie par la terrible (mais néammoins brillante) scientifique Rosie Zanie, qui a mené des expériences génético-magique pour créer une armée de cochons (plus ou moins) intelligent et envahir le monde avec. C'est ainsi qu'elle devenu impératrice de l'Imperium Porcorum. Rapidement, elle a démonté les institutions en cours, et toute sa vie, elle a cherché les secrets de la toute puissances... Cependant, on ne sût jamais ce qu'elle trouva, parce qu'à la fin de son règne, elle s'enferma dans la *Tour Zanie* et n'en ressorti jamais.

Depuis, le monde est divisé en 5 grandes régions, dont quatre dirigé par des Princeps cherchant chacun à prendre le pouvoir et devenir le nouvel empereur des cochons. Partout dans le monde, des généraux dirige une armée de cochons, et luttent entre eux pour cela pour obtenir le pouvoir. Après qu'elle se soit retiré dans la *Tour Zanie*, l'empire n'a plus eu d'empereurs et est resté sous la direction d'un conseils, et de 4 *Princeps* luttant pour un jour devenir les empereurs...

AVEC DES ARMÉES DE COCHONS BIEN SÛR.

Cet univers est à la base plus un gros délire qu'autre chose, avec une inspiration jeux-video. Il était prévu d'être utilisé dans un tactical RPG avec aspect "monster capture" ou on capturerais des cochons, puis d'autres animaux de fermes, pour partir au combat.
