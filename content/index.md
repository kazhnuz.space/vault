---
layout: layouts/base.njk
eleventyNavigation:
  key: Accueil
  order: 0
---

# {{ metadata.title }}

Bienvenue dans mon petit grenier ou je met tout mes vieux trucs !

Cette page est une petite page pour organiser un peu tout mon bazar et rendre plus simple à lire les vieux trucs que je voudrait poster. Vous y trouverez à la fois des vieux projets, des vieux concepts d'histoire, et d'autres petits trucs que j'ai fait pour le fun.

Tous ces contenus sont utilisable sous les termes CC BY-SA. (Creative Common Attribution - Partage à l'Identique). Certains de ces trucs reprennent cependant des éléments privateurs (venant de licences de JV, par exemple) en tant que fanfictions. Les aspects opensources ne sont que ceux que j'ai inventés. Tout n'est pas forcément utilisable (certains vieux trucs j'ai du utiliser une vieille VM avec un GM8 cracké pour pouvoir les retester)

Ce site utilise Eleventy

## Ce que vous pourrez trouver ici

- Des vieux concepts de projets
- Peut-être du vieux code
- Des vieux textes et constructions d'univers que j'ai mis de côté.
- En vrai pas grand chose d'intéressant, c'est surtout pour garder mes vieux trucs quelque part si je les reprends.

## Updates and history

- (**2023-06-09**) Relancement du site
- (**2021-01-24**) Lancement initial du site

## Crédits

- Site généré grâce à [Eleventy](11ty.org)
- Icone provenant de la version 2.9 du pack d'icone de GNOME
- Fond d'écran venant originellement de GNOME
- Design inspiré du theme lilac de Windows 95