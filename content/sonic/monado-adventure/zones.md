---
layout: layouts/base.njk
models:
  - SMA Informations
  - SMA Personnages
tags:
  - SMA Informations
title: Zones
parent: Sonic Monado
---

# Zones

Monado Prime est un système écologique en circuit fermé situé sur l'île désertique de Fortitude Rock. Cette base est composée d'un espace de vie principal, d'usine bactérienne biologique (s'occupant du plus gros de recyclage de l'air et des déchets biologiques), de biomes artificiels, d'un espace numérique autonome, ainsi que de tout un espace de laboratoires et de stations technologique. Il s'agit de la première station (d'où le nom Prime) de type Monado. Le terme monado indique le principe de base d'un écosystème clos : être un monde hermétique, une sorte d'univers fermé, tel les monades de Leibniz.

La base est composé d'une zone principale (Hermetic Utopia, notée MP0), de quatre biômes artificiels (l'Agrosphère, la Phytosphère, l'Hydrosphère, la Thermosphère, la Cryosphère, la Limnisphère et la Lithosphère, notés de MP1 à MP4) et de deux secteur interdits (la Gosth Central et l'Hybris Altar, noté MPY à MPZ).

Chaque zone est constitué d'un Adventure Field (à la Sonic Advance 3, avec des anneau d'acts et une sorte de capsule de boss, et quelques mini-jeux), et de 3 à 4 Action Stage servant d'acts. Ces acts sont constituée de deux "génériques" (suivant de manière proche le biome de la zone), et d'un ou deux plus spéciaux.

## MP0 - Hermetic Utopia, la zone habitée

Hermetic Utopia est la zone principale de la base, constituée d'un immense anneau entourant les quatre biomes artificiels, accessible via des grandes portes. C'est l'habitation d'une grande partie des habitants de la base, avec la phytosphère. Elle est composé d'une zone de ville en bas, et d'un système de metro en haut permettant d'accéder différemment aux zones (pour le joueur, il s'agit surtout d'un action stage).

Elle permet d'accéder aussi à l'extérieur (un petit HUB sans vraiment d'action, qui permettra surtout d'accéder au Tornado pour la phase Sky Chase et à la cinématique finale une fois le jeu terminé accompli).

Cette endroit ne possède pas de Chao Garden

### Adventure Field

L'Adventure Field de Hermetic Utopia est un immense anneau, et du coup est constitué d'une map se répétant sur les côtés (on peut faire le tour de l'anneau et revenir à son point de départ), avec quatre grandes portes vers les quatre biomes, et d'autres accès vers les autres zones.

### Action Stages

- **Wrath Corridor** : Un corridor militarisé entre le

- **Downtown Panic** : Les robots d'Eggman ont envahi certains quartiers ! Il va falloir les combattre pour atteindre le bout de cette route.

- **Crystal Railways** : Une immense route/tramway, qui permet notamment de traverser les grands espaces aeriens de l'anneau.

- **Erinyes Satellite** (accès) : Le plus grand des satellites de la gamme Erinyes, des satellites militaires de GUN qui étaient censer fonctionner avec l’énergie produite par la centrale.

### Accès à

- [via Wrath Corridor] Erebian Ruins ou Gosth Central

- Phytosphere

- Cryosphere

- Thermosphere

- Hydrosphere


## MP1 - La Phytosphère

*La sphère de la végétation, des grandes forêts et des plaines agricoles. Cette sphère a pour but de recréer un espace tempéré commun et simple. Plus qu'une sphère d'expérimentation, elle a pour but de permettre de nourrir toute la base, même si d'autres types d'aliment sont produit dans les autres sphères. Un petit village forestier, Woody Village, s'y trouve.*

### Adventure Field

L'Adventure Field de la Phytosphere est *Woody Village*, un petit village forestier.

### Action Stages

- **Lush Forest** : Une forêt artificielle, faisant partie des tests d'environnement du projet Monado. Cette forêt est parfaite pour y cacher des choses…

- **Verdant Plains** : De grande plaines, parfaite pour visiter la base ! Cette expérimentation aide aussi à pouvoir faire pousser de quoi nourrir toute la base.

- **Sylvian Base** : Une petite base d'Eggman, que Sonic pourra traverser pour en découvrir plus sur les plans du docteur. Son entrée est cachée dans la forêt.

- **Diké Trials** : Une pièce secrète crée par Morrigan, modelée suivant les légendes des anciennes épreuves de justice que subissait les membres de la civilisation erebienne pour monter en grade. Permet d'aider à bloquer l'énergie chaotique.

## MP2 - Hydrosphere

La sphère de l'océan, des mers et également des îles cotières. Si l'objectif de cette sphère est avant-tout de recréer un écosystème marin, elle est rapidement devenu également la sphère parfaite pour passer ses vacances. Qui croirait qu'il existe tout un paradis côtier au milieu de l'ile de Fortitude Rock ?

### Adventure Field

Un petit espace côtier, amenant vers les différents niveaux.

### Action Stages

- **Radiant Coast** : S'il s'agit d'un test d'environnement côtié, cette endroit est avant tout l'endroit.

- **Tropical Ocean** : Un ocean artificiel (plus proche d'une petite mer, en fait) qui est utilisée pour expérimenter les environnement marins. C'est vers cet endroit que se trouve une entrée submergée vers la Gosth Central, via un ancien conduit d'énergie.

- **Sophia Trials** : Une pièce secrète crée par Morrigan, modelée suivant les légendes des anciennes épreuves de sagesse que subissait les membres de la civilisation erebienne pour monter en grade. Permet d'aider à bloquer l'énergie chaotique. Elle permet également d'activer l'entrée vers le réacteur.

## MP3 - La Thermosphere

La sphère des déserts, de l'aridité et des environnements plus difficile. Beaucoup ne voyant qu'un aspect pratique à la biosphère se demande pourquoi une telle sphère existe. Bien plus scientifique du coup que les autres sphères, elle est également la sphère des laboratoires

### Adventure Field

Un petit espace désertique, amenant vers les différents niveaux.

### Action Stages

- **Arid Hills** : De grande collines désertiques, représentant les gransd déserts arides de sable.

- **Windy Cliff** : Un environnement représentant un désert plutôt venteux et montagneux.

- **Andreia Trials** : Une pièce secrète crée par Morrigan, modelée suivant les légendes des anciennes épreuves de courage que subissait les membres de la civilisation erebienne pour monter en grade. Permet d'aider à bloquer l'énergie chaotique.

## MP4 - La Cryosphère

La sphère des grands froids et de la neige. En effet, même si techniquement l'extérieur de la base est également glacial, Fortitude Rock est également aride. De plus, le but de la biosphère étant d'être un écosystème entier hermétique, il n'est pas possible d'avoir quelque chose à l'extérieur. C'est ici que se trouve la nouvelle centrale

### Adventure Field

Un petit espace glacial, amenant vers les différents niveaux.

### Action Stages

- **Diamound Mountain** : Une montagne artificielle.

- **Cryogenesis Grotto** : Une grotte glaciale.

- **Soprosyne Trials** : Une pièce secrète crée par Morrigan, modelée suivant les légendes des anciennes épreuves de tempérence que subissait les membres de la civilisation erebienne pour monter en grade. Permet d'aider à bloquer l'énergie chaotique.

## MPY - Gosth Central

L'ancienne centrale chaotique de la base, aujourd'hui désaffecté… Enfin, ça c'était avant qu'Eggman en fasse sa base principale de contrôle, profitant des portes dérobée installé par le général de GUN de l'époque pour s'assurer que personne d'autre que lui en prenne le contrôle, qui lui ont *mystérieusement été fournie*.

### Adventure Field - Gosthopolis

Un quartier "habité" de la ville d'Eggman. Contient quelques gens ayant suivit Eggman, et des blagues sur une propagande expliquant qu'il fait bon vivre dans l'empire d'Eggman.

### Action Stages

- **Spectral Archive** : Une base militaire du GUN, submergée sous des tonnes d’eau. Elle contient des archives sur le projet Gosth Central. De nombreuses informations importantes sont dedans.

- **Egg Dystopia** : Une immense ville construite par Eggman à l’intérieur même de la centrale. La ville exploite l’énergie de la centrale chaotique pour fonctionner.

- **Gosth Gadgets** : Le corridor principal d'énergie de la Gosth Central. Cette immense cylindre sert à charger et amplifier l’énergie de la veine chaotique jusqu’au Chaos Core. Ce niveau à un design vertical.

- **Chaos Core** : Cette immense sphère situé au fond du cylindre d'amplification est le point de saturation de l’énergie chaotique, essentiel à l’époque pour pouvoir ensuite la transformer en énergie pure réutilisable. L’énergie saturée est ensuite retransmise aux réacteurs pour la redistribution ensuite aux populations civiles et militaires. Étant constamment remplie d’énergie chaotique saturée, il s’agit d’un lieu dangereux.

## MPZ - Erebian Ruins

L'espace au cœur du centre de la base. L'ancien temple construit autour du Stygian Rift. Il est traversé par les tuyaux connectant la Gosth Central à la veine chaotique corrompue. La notion d'hybris évoque tout ceux qui ont pensé pouvoir contrôler cette source d'énergie incontrôlable.

C'est ici qu'est caché le serveur central de l'ile.

### Adventure Field

Les ruines de la civilisation erebienne, qui ont été transformée un peu technologiquement pour permettre la construction de la Gosth Central.

### Action Stages

- **Ancient Scriptures** : Les anciennes écritures du peuple erebien. La source de bien des légendes que Morrigan connait.

- **Hybris Altar** : Le lieu le plus important de la civilisation Erebienne. Cet autel, protégé du temps et des catastrophes, contient source principale de la Veine Chaotique.

- **Forgotten Void** : La Pocket Dimension se situant de l'autre côté de la faille chaotique, où arrivent toutes les personnes ayant subit le Egg Banisher. Il s'agit d'une sorte de mini "Special Zone".

- **Sillicon Discord** : Serveur central de l'île, caché dans les ruines.

## Boss

Le jeu possèdera un certains nombre de boss, en plusieur catégories.

### Les Chaos E-100

Des version améliorée des E-100 de Sonic Adventure (basé sur Chaos Gamma de Battle) qui seront des boss récurrant du jeu. Leur utilité est multiple. Premièrement, un peu de nostalgie :D Ensuite, cela sera un peu utilisé pour Amy, qui aura comme premier et dernier boss *Chaos Gamma*, ce qui lui permettra de mettre en avant l'aspect un peu terrible du sort de ces robots. La plupars n'auront qu'une variante (Beta, Delta, Epsilon, Zeta), mais Gamma aura deux variante (tout deux combattu par Amy).

Cela permet aussi d'avoir des boss un peu similaires mais différents, utiles pour certains passages du jeu.

"Chaos Alpha" sera également présent, comme boss final de Tails.

### Les mecha d'Eggman

Dans ce jeu, Eggman aura dans ce jeu deux mecha, un commandé à distance et un autre directement utilisé:

- Le Egg Charon

- Le Egg Banisher (boss final de Sonic)

### Autres

- Ys Gradlon (Boss final de Knuckles)

- Blood Hawk (Boss final de Shadow, mecha de GUN)

### Boss Final

**APATHY**, bataille contre Eggman.