---
layout: layouts/base.njk
eleventyNavigation:
  key: Sonic Xtreme
  parent: Sonic the Hedgehog
  order: 4
---

# Sonic Xtreme Remake

Comme beaucoup de jeunes fans de Sonic un peu dev, j'ai été fasciné par Sonic Xtreme quand j'étais plus jeune, et j'ai fait plusieurs tentatives de le recréer ou de partir de ses principes. J'ai fait quelques tentatives de recréation du jeu, chacune avec leurs qualités et défauts. J'avais plusieurs projets d'histoire, un qui mélangeait notamment pas mal d'élément de "titre oublié", et une autre ou je fusionnais toutes les histoires, notamment les trucs lié à la sorte de "Virus" qui contamine Sonic.

## La première

La première tentative avait un défaut particulier, le fait d'être en vue de derrière. Si cela peut parraitre pas tant à première vue, en fait cela provque pleins de complexité, et a fait que je savait que je devais passer en caméra fixe à côté de cela. Le moteur utilisait Game Maker avec un faux système de 3D pour les collisions : une collision 2D pour savoir si j'étais dans la zone de la plateforme, puis un check de la valeur z pour savoir ou je me situait sur l'axe en plus.

![Sonic, de dos, dans un terrain très "blocky" typé green hill](/img/xtreme/screen.png)

Plusieurs trucs m'ont fortement handicapé dans ce projet (outre mon manque de connaissance à l'époque mdr) :
- Les soucis de caméra (surtout que c'était full clavier avec juste une caméra tank, soit un peu incontrolable)
- Ce n'était pas possible de faire des éléments par dessus des autres
- Difficulté à gérer l'accélération encore plus avec les contrôle tank, du coup cela manquait de précision de manière dingue.

Mon plan pour gérer ça était de passer en caméra fixe, et de build les niveaux à partir de ça. Pour le second aspect, il me fallait un éditeur de niveau custom. Pour le premier aspect, le plan à rapidement évolue vers un autre projet, "Paper Sonic". 

## Editeur de niveaux

Un petit projet de création de niveau. Il générait un modèle qui serait utilisé par Game Maker avec un moteur de collision 3D que je comptais réutiliser. Ce projet a été plutôt fonctionnel en tant qu'éditeur, et l'export/import marchait pas trop mal.

![Un éditeur de niveau, pour faire des niveaux très blocky. Sonic y est visible de dos, quelques anneaux sont placé.](/img/xtreme/editor.png)

L'itnerface devait permettre de modifier la texture de l'appreil du block posé (c'était pas encore fonctionnel), et de poser différents types de blocs et objets

## Création d'un fisheye

En parallèle de ça, j'avais tenté de coder un système de fisheyes, basé sur un exemple de distortion d'écran pour Game Maker. À l'époque, je connaissais pas tout ce qui était vertex shader, donc ça avait été géré de manière plus dégueux.

![Un test de caméra en fisheyes, déformant les côté pour donner un aspect sphérique.](/img/xtreme/fisheyes.png)
