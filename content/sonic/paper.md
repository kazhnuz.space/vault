---
layout: layouts/base.njk
eleventyNavigation:
  key: Paper Sonic
  parent: Sonic the Hedgehog
  order: 5
---

# Paper Sonic

Le projet "Paper Sonic" était une évolution de mon projet Sonic Xtreme, ou j'avais un peu abandonné l'aspect Sonic Xtreme, pour tenter de réutiliser des sprites de megadrive dans un environnement 3D en caméra fixe. Ça devait être un jeu de plateforme avec des composantes action-RPG, à la manière de Super Paper Mario sur Wii, mais dans un environnement plus "blocky" à la Sonic Xtreme.

![Sonic, en sprite megadrive, dans un environnement blocky un peu futuriste/utopique](/img/xtreme/paper/utopia.png)

Le projet était fait en parallèle de celui Sonic Xtreme et les deux se nourissaient un peu. Il a été abandonné vers 2013, quand j'ai commencé à manquer de temps et n'avait plus de PC Windows pour utiliser Game Maker.

## Pourquoi ?

Passer de Sonic Xtreme à un Paper Sonic peut sembler, étrange, et ça vient d'une envie que j'avait et que je ne pouvais pas faire avec le jeu de base : ajouter du multi-personnages. L'idée du coup était qu'en utilisant les sprites des perso sur MD, ou en style MD, ce serait plus simple pour moi de faire du multi-personnage, là ou Xtreme n'avait des sprites que pour Sonic.

![Screenshot d'un menu de ou on voit deux niveau accessible, Techno Paradise et Floating stones, et Sonic, Tails et Knuckles jouable](/img/xtreme/paper/menu.png)

L'aspect plus RPG est venu rapidement, mais n'a jamais vraiment été implémenté. Le plus que j'ai mis, c'est un sprite de Shadow dans l'un des niveaux de tests.

![Sonic, face à Shadow dans un environnement de neige](/img/xtreme/paper/icelevel.png)

## Soucis rencontrés

Encore une fois, quelques soucis dans ce projet :
- Je galérait à l'époque à bien gérer mes courbes d'acceleration, du coup mes perso était super lourd et patinettes
- J'avais tendance à réfléchir le jeu en "vue de devant" alors qu'avec les sprites la vue de côté était de mise
- Les collisions étaient pleines de soucis, parce que j'avais séparé les "murs" et les sols, ce qui provoquait des collisions pas hyper bien finies

De plus, le code source s'est retrouvé dispersé dans deux fichier .gmk, (du à un ou j'expérimentait une refactorisation du moteur) ce qui fait que j'ai jamais vraiment pris le temps de tout réunir et adapter.

En plus, y'avait quelques bugs graphique sur la première version du moteur… qui était aussi celle qui avait le plus de contenu.

![Knuckles, face à un fragment de la Master Emerald dans un environnement désolé](/img/xtreme/paper/floating.png)
